import requests
import json
from requests.exceptions import ConnectionError
import pandas_datareader as pdr
import datetime
from slacker import Slacker
import matplotlib.pyplot as plt
import numpy as np

SLACK_WEBHOOK = "https://hooks.slack.com/services/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"
FAANG = ['FB', 'AMZN', 'AAPL', 'NFLX', 'GOOG']
SLACK_SECRET = 'xoxb-1320327005878-1344748838658-oanwIDFnYZSmalcyTSY4rtyK'
SLACK_CHANNEL = 'topic-python'
GRAPH_WINDOW = -120

def create_boll_data(df, att='Close', window_size=30):
    avg_name = str(window_size) + 'd mavg'
    std_name = str(window_size) + 'd std'
    cols = [avg_name,'Upper Band','Lower Band', att]    

    df[avg_name] = df[att].rolling(window=window_size).mean()
    df[std_name] = df[att].rolling(window=window_size).std()

    df['Upper Band'] = df[avg_name] + (df[std_name] * 2)
    df['Lower Band'] = df[avg_name] - (df[std_name] * 2)

    # if df[att].tail(1) > df["Upper Band"].tail(1):
    #     diff = df[att] - df["Upper Band"] 
    #     print("Market price for this ticker is " + diff + " above upper band. Consider selling!")
    # elif df[att].tail(1) < df["Lower Band"].tail(1):
    #     diff = df["Upper Band"] - df[att]
    #     print("Market price for this ticker is " + diff + " below lower band. Consider buying.")
    # else: print("You are in the right place. Keep the stock!")

    return df[cols].dropna()[GRAPH_WINDOW:]


def create_boll_plot(bol_df, title):
    # set style, empty figure and axes
    plt.style.use('fivethirtyeight')
    fig = plt.figure(figsize=(12,6))
    ax = fig.add_subplot(111)

    # Get index values for the X axis for the DataFrame
    x_axis = bol_df.index.get_level_values(0)

    print(x_axis)
    # Plot shaded 'win' Day Bollinger Band for Facebook
    ax.fill_between(x_axis, 
                    bol_df['Upper Band'], 
                    bol_df['Lower Band'], 
                    color='grey')

    ax.plot(x_axis, bol_df['Close'], color='blue', lw=2)
    ax.plot(x_axis, bol_df['30d mavg'], color='black', lw=2)

    # Set Title & Show the Image
    ax.set_title(title)
    ax.set_xlabel('Dates')
    ax.set_ylabel('Price (USD)')

    fig.savefig("plot.png")


def get_ticker_info(tickername):
    return pdr.get_data_yahoo(tickername)


if __name__ == "__main__":
    slack = Slacker(SLACK_SECRET)

    print("What ticker do you want?")
    user_input = input().upper().split(',')
    
    for ticker in user_input:
        ticker_info = get_ticker_info(ticker.strip())
        bol_df = create_boll_data(ticker_info)
        create_boll_plot(bol_df, 'Bollinger Graph for ' + ticker)
        # slack.chat.post_message('#' + SLACK_CHANNEL, ticker + ' info:')
        # slack.files.upload("plot.png", channels=SLACK_CHANNEL)
